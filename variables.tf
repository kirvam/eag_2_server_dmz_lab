##  First complete Variables file
##
## RESOURCE GROUP
variable "resource_group" {
     default = "eagdev02RG00"
 description = "Resource Group"
}


## EAST LOCATION
variable "usgovlocation" {
       default = "usgovvirginia"
}

#############################################################################################
## VNET
variable "vnet" {
     default = "eagdev02VNET0"
 description = "Vnet"
}

## VNET ADDRESS
variable "vnet_address" {
     default = ["10.251.2.0/24"]
}

## PUBLIC IP, PIP
variable "pip" {
     default = "PIP0"
 description = "Public IP"
}

############################################################################################
## SERVER NAME
variable "vm_name" {
      default = "eagvm0"
  description = "Standard EAG VM name"
}

## SUBNET NAME BASE
variable "subnet_name_base" {
     default = "dev02net0"
 description = "Standard Subnet Name Base"
}

variable "admin_username" {
    ## default = "phaigh"
    default = "infradmin"
 description = "First User"
}

variable "admin_password" {
     default = "Eag-dev02-truck-pear-tiger!"
 description = "Password"
}

variable "subnets_required" {
        type = "map"
     default = {
          "0" = "STG"
          STG = "STG"
          "1" = "PRD"
          PRD = "PRD"
          "2" = "DMZ"
          DMZ = "DMZ"
          "3" = "DEV"
          DEV = "DEV"
         }
}

variable "subnet_cidr" {
        type = "map"
      default = {
              STG = "10.251.2.0/26" 
              PRD = "10.251.2.64/26"
              DMZ = "10.251.2.128/26"
              DEV = "10.251.2.192/26"
         } 
}
