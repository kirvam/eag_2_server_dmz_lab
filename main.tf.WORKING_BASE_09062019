provider "random" {
  version = "~> 2.2"
}
provider "azurerm" {
#  version = "~> 2.2"
# Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.28.0"
## SLDS
##    subscription_id = "344408ae-21d5-4c3a-9b0e-cdcea56bbdcb"
## AOE-SDLC
      subscription_id = "1418ebd4-653b-44d7-8bec-77d302695636"
}

output "SUB_ID_AOE-SDLC" {
      value = "1418ebd4-653b-44d7-8bec-77d302695636"
description = "Subscription ID"
}

# Create a resource group if it doesn.t exist
resource "azurerm_resource_group" "tfgrp" {
      name     = "${var.resource_group}"
      location = "${var.usgovlocation}"
      tags = {
          ORG = "AOE"
          APP = "SDLC"          
         TYPE = "TEST"
       }
}

output "rg-name" {
  value = "${azurerm_resource_group.tfgrp.name}"
 description = "Name of Resource Group"
}

# Create virtual network
resource "azurerm_virtual_network" "tfnet" {
     name                = "${var.vnet}"
     address_space       = "${var.vnet_address}"
     location            = "${var.usgovlocation}"
     resource_group_name = "${azurerm_resource_group.tfgrp.name}"
}

output "vnet-name" {      
  value = "${azurerm_virtual_network.tfnet.name}"
 description = "Name of Vnet to be used by deplotment."
}

module "subnet_mod" {
       source = "./modules/vnets/subnets/"
       #resource_group_name  = "${azurerm_resource_group.tfgrp.name}"
         RGN = "${azurerm_resource_group.tfgrp.name}"
       #virtual_network_name = "${azurerm_virtual_network.tfnet.name}"
         VNN = "${azurerm_virtual_network.tfnet.name}"
         LOC = "${var.usgovlocation}"
}

output "STG_SUBNET" {
       value = module.subnet_mod.subnetSTG
 description = "STG Subnet"
}

output "STG_NSG" {
       value = module.subnet_mod.nsgSTG
 description = "STG NSG"
}

# KEEP FOR FUTURE TESTING
# Create public IPs
#resource "azurerm_public_ip" "tfpip" {
#    name                          = "${var.pip}-${count.index}"
#    location                      = "${var.usgovlocation}"
#    resource_group_name           = "${azurerm_resource_group.tfgrp.name}"
#    allocation_method             = "Dynamic"
#    count                         = 2
#
#    tags = {
#        ENV = "EAG DEMO"
#    }
#}


variable "nic_names" {
  description = "Create list of Nics to create."
  type        = list(string)
  default     = ["STG00","STG01","STG02","STG03"]
}

output "VM_NicNames" {
   value     = "${var.nic_names}"
 description = "List of VMs"
}

## Create NICs
## NSGs created here too.
##
module "nic_mod" {
       source = "./modules/vnets/subnets/network_interfaces"
          LOC = "${var.usgovlocation}"
        #COUNT = "3"
        NICNAMES = var.nic_names
          RGN = "${azurerm_resource_group.tfgrp.name}"
         NAME = "STG"
         NSGI = module.subnet_mod.nsgSTG
          SNI = module.subnet_mod.subnetSTG 
}

output "NIC_LIST_STG" {
       value = module.nic_mod.NIID
 description = "List of NICS"
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.tfgrp.name}"
    }
    byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "strstg" {
    name                        = "diag01${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.tfgrp.name}"
    location                    = "${var.usgovlocation}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        ENV = "AOE-SDLC"
    }
}

module "vm_mod" {
       source = "./modules/vnets/subnets/network_interfaces/vms"
         NAME = "STG"
          LOC = "${var.usgovlocation}"
          RGN = "${azurerm_resource_group.tfgrp.name}"
        #NIID -coming from network_interface
         NIID = module.nic_mod.NIID
        #COUNT = "3"
        NICNAMES = var.nic_names
       VMSIZE = "Standard_DS1_v2"
       VMNAME = "-app"
      ADMUSER = "${var.admin_username}"
      ADMPASS = "${var.admin_password}"
     # STRDISK = "${var.vm_name}${count.index}OsDisk" 
    BOOTSTRUI = "${azurerm_storage_account.strstg.primary_blob_endpoint}"
}

##########################################################################################
# Generate random text for a unique storage account name
#resource "random_id" "randomId" {
#    keepers = {
#        # Generate a new ID only when a new resource group is defined
#        resource_group = "${azurerm_resource_group.tfgrp.name}"
#    }
#    byte_length = 8
#}

# Create FileStorage storage account for Data 
resource "azurerm_storage_account" "data01" {
    name                        = "data01${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.tfgrp.name}"
    location                    = "${var.usgovlocation}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        ENV = "AOE-SDLC"
    }
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "strprd" {
    name                        = "diag02${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.tfgrp.name}"
    location                    = "${var.usgovlocation}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"
    
    tags = {
        ENV = "AOE-SDLC"
    }
}

variable "nic_names_prd" {
  description = "Create list of Nics to create."
  type        = list(string)
  default     = ["PRD00","PRD01","PRD02","PRD03"]
}

output "VM_NicNames_PRD" {
   value     = "${var.nic_names_prd}"
 description = "List of prd VMs"
}

## Create NICs
## NSGs created here too.
##
module "nic_mod_prd" {
       source = "./modules/vnets/subnets/network_interfaces"
          LOC = "${var.usgovlocation}"
        #COUNT = "3"
        NICNAMES = var.nic_names_prd
          RGN = "${azurerm_resource_group.tfgrp.name}"
         NAME = "PRD"
         NSGI = module.subnet_mod.nsgPRD
          SNI = module.subnet_mod.subnetPRD
}

output "NIC_LIST_PRD" {
       value = module.nic_mod_prd.NIID
 description = "List of PRD NICS"
}

module "vm_mod_prd" {
       source = "./modules/vnets/subnets/network_interfaces/vms"
         NAME = "PRD"
          LOC = "${var.usgovlocation}"
          RGN = "${azurerm_resource_group.tfgrp.name}"
        #NIID -coming from network_interface
         NIID = module.nic_mod_prd.NIID
        #COUNT = "3"
        NICNAMES = var.nic_names_prd
       VMSIZE = "Standard_B2s"
       VMNAME = "-app"
      ADMUSER = "${var.admin_username}"
      ADMPASS = "${var.admin_password}"
     # STRDISK = "${var.vm_name}${count.index}OsDisk"
    BOOTSTRUI = "${azurerm_storage_account.strprd.primary_blob_endpoint}"
}

