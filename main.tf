provider "random" {
  version = "~> 2.2"
}
provider "azurerm" {
#  version = "~> 2.2"
# Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.28.0"
# EAG-DEV02
        subscription_id = "344408ae-21d5-4c3a-9b0e-cdcea56bbdcb"
}

output "SUB_ID_EAGDEV02" {
      value = "344408ae-21d5-4c3a-9b0e-cdcea56bbdcb"
description = "Subscription ID"
}

## Create a resource group if it doesn.t exist
resource "azurerm_resource_group" "tfgrp" {
      name     = "${var.resource_group}"
      location = "${var.usgovlocation}"
      tags = {
          ORG = "EAG"
          APP = "Public facing App"          
         TYPE = "TEST"
       }
}

output "rg-name" {
  value = "${azurerm_resource_group.tfgrp.name}"
 description = "Name of Resource Group"
}

## Create virtual network
resource "azurerm_virtual_network" "tfnet" {
     name                = "${var.vnet}"
     address_space       = "${var.vnet_address}"
     location            = "${var.usgovlocation}"
     resource_group_name = "${azurerm_resource_group.tfgrp.name}"
}

output "vnet-name" {      
  value = "${azurerm_virtual_network.tfnet.name}"
 description = "Name of Vnet to be used by deployment."
}

##  Create Subnets
##  -Go to Module directory...
##
##  NSGs mad when subnets are made...
module "subnet_mod" {
       source = "./modules/vnets/subnets/"
       #resource_group_name  = "${azurerm_resource_group.tfgrp.name}"
         RGN = "${azurerm_resource_group.tfgrp.name}"
       #virtual_network_name = "${azurerm_virtual_network.tfnet.name}"
         VNN = "${azurerm_virtual_network.tfnet.name}"
         LOC = "${var.usgovlocation}"
}

## Publish Subnets Outputs
##
output "STG_SUBNET" {
       value = module.subnet_mod.subnetSTG
 description = "STG Subnet"
}

output "STG_NSG" {
       value = module.subnet_mod.nsgSTG
 description = "STG NSG"
}

output "PRD_SUBNET" {
       value = module.subnet_mod.subnetPRD
 description = "PRD Subnet"
}

output "PRD_NSG" {
       value = module.subnet_mod.nsgPRD
 description = "PRD NSG"
}

output "DMZ_SUBNET" {
       value = module.subnet_mod.subnetDMZ
 description = "DMZ Subnet"
}

output "DMZ_NSG" {
       value = module.subnet_mod.nsgDMZ
 description = "DMZ NSG"
}



## KEEP FOR FUTURE TESTING
## Create public IPs
#resource "azurerm_public_ip" "tfpip" {
#    name                          = "${var.pip}-${count.index}"
#    location                      = "${var.usgovlocation}"
#    resource_group_name           = "${azurerm_resource_group.tfgrp.name}"
#    #allocation_method             = "Dynamic"
#    allocation_method             = "Static"
#
#    count                         = 1
#
#    tags = {
#        ENV = "EAG"
#    }
#}
############################################################################################
## These are the Basic 3 elements to a server, Publisher, Offer, and SKU:
## variable "VPUB" {}
## variable "VOFFER" {}
## variable "VSKU" {}
############################################################################################

variable "vmpublisherWIN" {
  description = "VM WIN Publisher"
      default = "MicrosoftWindowsServer"  
}  

variable "vmpublisherUbuntu" {
  description = "VM Ubuntu Publisher"
      default = "Canonical"  
}

variable "vmpublisherCentos" {
  description = "VM Centos Publisher"
      default = "OpenLogic"  
}
##
variable "vmofferWIN" {
  description = "VM WIN Offer"
      default = "WindowsServer"  
}

variable "vmofferUbuntu" {
  description = "VM Ubuntu Offer"
      default = "UbuntuServer"  
}

variable "vmofferCentos" {
  description = "VM Centos Offer"
      default = "CentOS"  
}
##
variable "vmskuWIN" {
  description = "VM WIN 2019 DC SKU"
      default = "2019-Datacenter"  
}

variable "vmskuUbuntu" {
  description = "VM Ubuntu SKU"
      default = "18.04-LTS"  
}

variable "vmskuCentos" {
  description = "VM Centos SKU"
      default = "7.5"  
}
############################################################################################
## Examples of some standard GOV server sizes:
##
## Standard_B2s	
## Standard_B4ms	
## Standard_B4ms	
## Standard_B4ms	
## Standard_B4ms	
## Standard_B4ms	
##
############################################################################################
## Crreating Availability Set
##
resource "azurerm_availability_set" "aoe_avset_stg" {
    name = "aoe_avset_stg"
    location = "${var.usgovlocation}"
    resource_group_name = "${azurerm_resource_group.tfgrp.name}"
    platform_fault_domain_count  = 2
    platform_update_domain_count = 2
    managed                      = true
}
resource "azurerm_availability_set" "aoe_avset_prd" {
    name = "aoe_avset_prd"
    location = "${var.usgovlocation}"
    resource_group_name = "${azurerm_resource_group.tfgrp.name}"
    platform_fault_domain_count  = 2
    platform_update_domain_count = 2
    managed                      = true
}
#resource "azurerm_availability_set" "aoe_avset_dmz" {
#    name = "aoe_avset_dmz"
#    location = "${var.usgovlocation}"
#    resource_group_name = "${azurerm_resource_group.tfgrp.name}"
#    platform_fault_domain_count  = 2
#    platform_update_domain_count = 2
#    managed                      = true
#}
## availability_set_id   = "${azurerm_availability_set.aoe_avset_stg.id}"
## AVSET = "${azurerm_availability_set.aoe_avset_stg.id}"
## availability_set_id   = "${azurerm_availability_set.aoe_avset_prd.id}"
## AVSET = "${azurerm_availability_set.aoe_avset_prd.id}"
##
############################################################################################
## MAKING STG Servers
##
############################################################################################
## Creating Variables for NICs
##
variable "nic_names" {
  description = "Create list of Nics to create."
  type        = list(string)
  default     = ["stg-web01","stg-web02"]
}

output "VM_NicNames" {
   value     = "${var.nic_names}"
 description = "List of VMs"
}


## Create NICs
## NSGs created here too.
##
module "nic_mod" {
       source = "./modules/vnets/subnets/network_interfaces"
          LOC = "${var.usgovlocation}"
     NICNAMES = var.nic_names
          RGN = "${azurerm_resource_group.tfgrp.name}"
         NAME = "STG"
         NSGI = module.subnet_mod.nsgSTG
          SNI = module.subnet_mod.subnetSTG 
}

output "NIC_LIST_STG" {
       value = module.nic_mod.NIID
 description = "List of NICS"
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
      # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.tfgrp.name}"
    }
    byte_length = 8
}

##
## Creating storage account for boot diagnostics
##
resource "azurerm_storage_account" "strstg" {
    name                        = "diag01${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.tfgrp.name}"
    location                    = "${var.usgovlocation}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        ENV = "EAG-DEV02"
       TYPE = "LAB"
        ORG = "EAG"
    }
}

##
## Create DISKS
##
#resource "azurerm_managed_disk" "osdisk" {
#  create_option         = "FromImage"
#}
#
#resource "azurerm_virtual_machine" "test" {
#    delete_os_disk_on_termination = false
#    storage_os_disk {
#        create_option      = "attach"
#        managed_disk_id = "${azurerm_managed_disk.osdisk.id}"
#    }
#}

## Create VM
module "vm_mod" {
      #source = "./modules/vnets/subnets/network_interfaces/vms"
       source = "./modules/vnets/subnets/network_interfaces/vms/Lin"
         NAME = "STG"
        AVSET = "${azurerm_availability_set.aoe_avset_stg.id}"
          LOC = "${var.usgovlocation}"
          RGN = "${azurerm_resource_group.tfgrp.name}"
        #NIID -coming from network_interface
         NIID = module.nic_mod.NIID
      NICNAMES = var.nic_names
     ## 2 core, 2 GB memory, 104 GB Drive ##
       VMSIZE = "Standard_B1ms" 
        VMPUB = var.vmpublisherCentos
      VMOFFER = var.vmofferCentos
        VMSKU = var.vmskuCentos
       VMNAME = "DkrWeb"
      ADMUSER = "${var.admin_username}"
      ADMPASS = "${var.admin_password}"
     # STRDISK = "${var.vm_name}${count.index}OsDisk" 
    BOOTSTRUI = "${azurerm_storage_account.strstg.primary_blob_endpoint}"
}
############################################################################################
## START VM BLOCK
############################################################################################

