## Create subnet STG 
resource "azurerm_subnet" "subnetSTG" {
    name                 = "${var.subnets_required.STG}"
     resource_group_name = var.RGN
    virtual_network_name = var.VNN
    address_prefix       = "${var.subnet_cidr.STG}"
}
    

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsgSTG" {
     name                = "nsgSTG"
     location            = var.LOC
     resource_group_name = var.RGN

     security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    tags = {
         ORG = "EAG"
         ENV = "EAG-DEV02"
        TYPE = "LAB"
    }
}

output "nsgSTG" {
       value = "${azurerm_network_security_group.nsgSTG.id}"
 description = "STG NSG"
}
#module.subnets.nsgSTG
 
output "subnetSTG" {
       value = "${azurerm_subnet.subnetSTG.id}"
 description = "STG Subnet"
}
#module.subnets.subnetSTG

## Create subnet PRD 
resource "azurerm_subnet" "subnetPRD" {
    name                 = "${var.subnets_required.PRD}"
     resource_group_name = var.RGN
    virtual_network_name = var.VNN
    address_prefix       = "${var.subnet_cidr.PRD}"
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsgPRD" {
      name                = "nsgPRD"
      location            = var.LOC
      resource_group_name = var.RGN

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    tags = {
        ORG = "EAG"
        ENV = "EAG-DEV02"
        TYPE = "LAB"
    }
}

output "nsgPRD" {
       value = "${azurerm_network_security_group.nsgPRD.id}"
 description = "PRD NSG"
}
#module.subnets.nsgPRD

output "subnetPRD" {
       value = "${azurerm_subnet.subnetPRD.id}"
 description = "PRD Subnet"
}
#module.subnets.subnetPRD


## Create subnet DMZ 
resource "azurerm_subnet" "subnetDMZ" {
    name                 = "${var.subnets_required.DMZ}"
     resource_group_name = var.RGN
    virtual_network_name = var.VNN
    address_prefix       = "${var.subnet_cidr.DMZ}"
}
output "nsgDMZ" {
       value = "${azurerm_network_security_group.nsgDMZ.id}"
 description = "DMZ NSG"
}
#module.subnets.nsgDMZ

output "subnetDMZ" {
       value = "${azurerm_subnet.subnetDMZ.id}"
 description = "DMZ Subnet"
}
#module.subnets.subnetDMZ


###
resource "azurerm_network_security_group" "nsgDMZ" {
      name                = "nsgDMZ"
      location            = var.LOC
      resource_group_name = var.RGN

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22" 
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    tags = {
         ORG = "EAG"
         ENV = "EAG-DEV02"
        TYPE = "LAB"
    }
}
###

## Create subnet TEST 
resource "azurerm_subnet" "subnetTEST" {
    name                 = "${var.subnets_required.TEST}"
     resource_group_name = var.RGN
    virtual_network_name = var.VNN
    address_prefix       = "${var.subnet_cidr.TEST}"
}

