variable "RGN" {}
variable "VNN" {}
variable "LOC" {}

variable "subnets_required" {
        type = "map"
     default = {
          "0" = "STG"
          STG = "STG"
          "1" = "PRD"
          PRD = "PRD"
          "2" = "DMZ"
          DMZ = "DMZ"
          "3" = "TEST"
         TEST = "TEST"
         }
}

variable "subnet_cidr" {
        type = "map"
      default = {
              STG = "10.251.2.0/26"
              PRD = "10.251.2.64/26"
              DMZ = "10.251.2.128/26"
             TEST = "10.251.2.192/26"
         }
}

