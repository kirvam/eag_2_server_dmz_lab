

# Create network interface
#resource "azurerm_network_interface" "tfnicSTG" {
resource "azurerm_network_interface" "tfnic" {

     ##count                     = var.COUNT
     #name                      = "NIC${count.index}"
     ## name                      = "${var.NAME}-NIC-${count.index}"
         count                  = length(var.NICNAMES)
         name                   = var.NICNAMES[count.index]

    #location                  = "${var.usgovlocation}"
     location                  = var.LOC
    #resource_group_name       = "${azurerm_resource_group.tfgrp.name}"
     resource_group_name       = var.RGN
    #network_security_group_id = "${azurerm_network_security_group.tfnsg.id}"
    network_security_group_id = var.NSGI
    # network_security_group_id = module.subnet_mod.nsgSTG

    ip_configuration {
        #name                          = "tstNicConfig"
        #name                          = var.NAME
         name                          = "nic.var.NAME"
        #subnet_id                     = "${azurerm_subnet.tfsubnet01.id}"
        subnet_id                     = var.SNI
        # subnet_id                     = module.subnet_mod.subnetSTG
         
         private_ip_address_allocation = "Dynamic"
#public_ip_address_id         = "${length(azurerm_public_ip.tfpip.*.id) > 0 ? element(concat(azurerm_public_ip.tfpip.*.id, list("")), count.index) : ""}"
         #public_ip_address_id         = var.PIPA
    }
    tags = {
        ENV = var.NAME
    }
}

output "NIID" {
   #value = "${azurerm_network_interface.tfnicSTG.id}"
   #network_interface_ids = ["${element(azurerm_network_interface.tfnicSTG.*.id, count.index)}"]
   value = azurerm_network_interface.tfnic
}
