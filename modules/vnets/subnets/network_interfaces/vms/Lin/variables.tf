# vm variables
variable "NAME" {}
variable "LOC" {}
variable "RGN" {}
variable "NIID" {}
#variable "COUNT" {}
variable "NICNAMES" {}
variable "VMSIZE" {}
variable "VMNAME" {}
variable "ADMUSER" {}
variable "ADMPASS" {}
variable "BOOTSTRUI" {}
variable "VMPUB" {}
variable "VMOFFER" {}
variable "VMSKU" {}
variable "AVSET" {}
##variable "LINOsDiskIDs" {}




#variable "SNI" {}
#variable "PIPA" {}
#variable "NIC" {}

variable "subnets_required" {
        type = "map"
     default = {
          "0" = "STG"
          STG = "STG"
          "1" = "PRD"
          PRD = "PRD"
          "2" = "DMZ"
          DMZ = "DMZ"
          "3" = "TEST"
         TEST = "TEST"
         }
}

variable "subnet_cidr" {
        type = "map"
      default = {
              STG = "10.251.3.0/26"
              PRD = "10.251.3.64/26"
              DMZ = "10.251.3.128/26"
             TEST = "10.251.3.192/26"
         }
}

#variable "STG" {
#  default = "STG"
#  default = "${var.subnets_required.STG}"
#}

#output "subnetSTG" {
#  value = "${var.STG}"
#}

