# vms
############################################################################################
##
############################################################################################
# Create DISKS
##
#resource "azurerm_managed_disk" "osdisk" {
#  create_option         = "FromImage"
#          count         = length(var.NICNAMES)
#          name          = var.NICNAMES[count.index]
#        location        = var.LOC
#    resource_group_name = var.RGN
#   storage_account_type = "Standard_LRS"
#}

#output "LINOsDiskIDs" {
#  value = ["${azurerm_managed_disk.osdisk}"]
#}

#resource "azurerm_virtual_machine" "disk" {
#    delete_os_disk_on_termination = false
#    storage_os_disk {
#        create_option      = "attach"
#        managed_disk_id = "${azurerm_managed_disk.osdisk.id}"
#    }
#}

############################################################################################
# Create virtual machine
resource "azurerm_virtual_machine" "tfvmlin" {
      location              = var.LOC
      count                 = length(var.NICNAMES)
      name                  = var.NICNAMES[count.index]
      availability_set_id   = var.AVSET
        resource_group_name = var.RGN
      network_interface_ids = ["${element(var.NIID.*.id, count.index)}"]
#      ## Should be ^"${element(var.NIID.*.id, count.index)}"^ instead?? "]]" are for a list..  ###
      vm_size               = var.VMSIZE

    storage_os_disk {
      #   name              = "${var.vm_name}${count.index}OsDisk"
      #   name              = "${var.NAME}${count.index}OsDisk"
      #   name              = "${var.NICNAMES[count.index]}${var.NAMES}${count.index}OsDisk"
      #   name              = "${var.NICNAMES[count.index]}${count.index}OsDisk"
          name              = "${var.NICNAMES[count.index]}Disk"
        
         ##  create_option      = "attach"
         ##   managed_disk_id = "${azurerm_managed_disk.osdisk.id}"
         ##   managed_disk_id = ["${element(azurerm_managed_disk.osdisk.*.id, count.index)}"]
         ##   managed_disk_id = "${azurerm_managed_disk.osdisk[count.index]}"
         ##   managed_disk_id = ["${element(var.LINOsDiskIDs.*.id, count.index)}"]
            caching           = "ReadWrite"
            create_option     = "FromImage"
            managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        #publisher = "Canonical"
         publisher = var.VMPUB
        #offer     = "UbuntuServer"
             offer = var.VMOFFER
         #sku      = "16.04.0-LTS"
               sku = var.VMSKU
         version   = "latest"
   }

    os_profile {
#        computer_name  = "radical"
         computer_name  = "${var.NICNAMES[count.index]}${var.VMNAME}${count.index}"
        admin_username = "${var.ADMUSER}"
        admin_password = "${var.ADMPASS}"
    }

os_profile_linux_config {
         disable_password_authentication = false
    }
#os_profile_windows_config {
#
#}
    boot_diagnostics {
        enabled = "true"
        #storage_uri = "${azurerm_storage_account.storageacct.primary_blob_endpoint}"
        storage_uri = var.BOOTSTRUI
    }

    tags = {
        ORG = "EAG"
       TYPE = "LAB"
        ENV = "EAG-DEV02"
    }
}
