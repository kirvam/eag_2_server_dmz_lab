# vms

# Create virtual machine
resource "azurerm_virtual_machine" "tfvm" {
     #name                  = "${vandexr.NAME}${count.index}"
     #location              = "${var.usgovlocation}"
      location              = var.LOC
      count                 = length(var.NICNAMES)
      name                  = var.NICNAMES[count.index]
    
     #resource_group_name   = "${azurerm_resource_group.tfgrp.name}"
      resource_group_name = var.RGN

     #network_interface_ids = ["${element(azurerm_network_interface.tfnic.*.id, count.index)}"]
#      network_interface_ids = ["${element(var.NIID.*.id, count.index)}"]
      network_interface_ids = ["${element(var.NIIDP.*.id, count.index)}"]


    # vm_size               = "Standard_DS1_v2"
      vm_size               = var.VMSIZE
    # count                 = var.COUNT

    storage_os_disk {
      #  name              = "${var.vm_name}${count.index}OsDisk"
      #   name              = "${var.NAME}${count.index}OsDisk"
      #   name              = "${var.NICNAMES[count.index]}${var.NAMES}${count.index}OsDisk"
      #   name              = "${var.NICNAMES[count.index]}${count.index}OsDisk"
          name              = "${var.NICNAMES[count.index]}${count.index}_OsDisk"

        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
#        computer_name  = "radical"
         computer_name  = "${var.NICNAMES[count.index]}${var.VMNAME}${count.index}"
        admin_username = "${var.ADMUSER}"
        admin_password = "${var.ADMPASS}"
    }
#    os_profile_linux_config {
#        disable_password_authentication = true
#        ssh_keys {
#            path     = "/home/phaigh/.ssh/authorized_keys"
#            key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCpOfCtnQGFqRxoSp7qNnCH372UjU1LiDNmQoqSmr1HGu2Spt2YWPCUrDyDBRsg5HXHVQWAOJbhHZnKd+3UzzAfMGYCvirNZFGE1XDo1tsBTM3vAqEqIgzsYmMWikkWxeL7vSOTeL3V0AlYjcE2m5Cs1wtraZE+hqSJ0jr6Fj3HNbhKEyvUeMQnLQQ4w8ln3R67Rp5Z1fWjgXe5wq734YHc2A0v4kfdCnALO61oGnwXiFTtnWDf5n45A3/fypQbafllr5bVw6wr3P7eL18rNg8qeJyJCvzk2aTrMVwHAq7gqBq5vkH198/BdNCI4HD4fJFycG2ryfi1WWdoTG5oRXXh"
#        }
#    }

os_profile_linux_config {
         disable_password_authentication = false
    }

    boot_diagnostics {
        enabled = "true"
        #storage_uri = "${azurerm_storage_account.storageacct.primary_blob_endpoint}"
        storage_uri = var.BOOTSTRUI
    }

    tags = {
        ENV = "AOE-SDLC"
    }
}
