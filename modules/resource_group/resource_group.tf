# Create a new resource group
#resource "azurerm_resource_group" "rg" {
#    name     = "${var.ARM_AZ_RGNAME}"
#    location = "${var.ARM_AZ_LOCATION}"
#
#    tags  {
#        DESC = "${var.tag_desc}"
#         ENV = "${var.tag_env}"
#        TYPE = "${var.tag_type}"
#         OWN = "${var.tag_own}"
#         GRP = "CS"
#    }
#
#}
# Create a resource group if it doesn.t exist
resource "azurerm_resource_group" "tfgrp" {
      name     = "${var.resource_group}"
       location = "${var.usgovlocation}"
      tags = {
          ORG = "EAG"
         TYPE = "TEST"
          OWN = "PMH"
       }
}


output "rg-name" {
    value = "${azurerm_resource_group.tfgrp}"
}

